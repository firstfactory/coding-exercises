﻿using System;
namespace Exercises.CesarCypher
{
    public static class CesarCypher
    {
        static readonly string abcUpper = "EFGHABCDPQRSIJKLWXYZMNOTUV1234567890";
        static readonly string abcLower = "efghabcdpqrsijklwxyzmnotuv1234567890";

        public static string Execute(string message, int moves, bool cypher = true)
        {
            string response = "";
            var positions = moves >= abcUpper.Length ? moves - abcUpper.Length : moves;
            if (positions > 0 && positions < abcUpper.Length)
            {
                for (int i = 0; i < message.Length; i++)
                {
                    int posCaracterUpper = GetPosABC(message[i], abcUpper);
                    int posCaracterLower = GetPosABC(message[i], abcLower);
                    if (Math.Max(posCaracterLower, posCaracterUpper) == -1) response += message[i];
                    else
                    {
                        if (cypher)
                            response = Cypher(positions, response, posCaracterUpper, posCaracterLower);
                        else
                            response = Decypher(positions, response, posCaracterUpper, posCaracterLower);
                    }
                }
            }
            return response;
        }

        private static string Cypher(int positions, string response, int posCaracterUpper, int posCaracterLower)
        {
            var abc = posCaracterUpper > posCaracterLower ? abcUpper : abcLower;
            int pos = Math.Max(posCaracterUpper, posCaracterLower) + positions;
            while (pos >= abc.Length)
            {
                pos -= abc.Length;
            }
            response += abc[pos];
            return response;
        }

        public static string Decypher(int positions, string response, int posCaracterUpper, int posCaracterLower)
        {
            var abc = posCaracterUpper > posCaracterLower ? abcUpper : abcLower;
            int pos = Math.Max(posCaracterUpper, posCaracterLower) - positions;
            while (pos < 0)
            {
                pos += abc.Length;
            }
            response += abc[pos];
            return response;
        }

        static int GetPosABC(char caracter, string abc)
        {
            return abc.IndexOf(caracter);
        }

        public static void Start()
        {
            var message = Console.ReadLine();
            var positions = int.Parse(Console.ReadLine());
            var cyphered = Execute(message, positions);
            var decyphered = Execute(cyphered, positions, false);

            Console.WriteLine($"Message cyphered is {cyphered}");
            Console.WriteLine($"Message decyphered is {decyphered}");
        }
    }
}
