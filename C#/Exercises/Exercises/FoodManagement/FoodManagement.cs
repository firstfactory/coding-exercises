﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Exercises.FoodManagement
{
    public static class FoodManagement
    {
        private class BestEvaluation
        {
            public string Name { get; set; }
            public double Score { get; set; }
        }

        public static string GetHighestRating(List<Recipe> recipes, string category)
        {
            var bestEvaluations = recipes.Select(x => new BestEvaluation
            {
                Name = x.Name,
                Score = x.Evaluations.Average(evaluation => category.Equals("Presentation") ? evaluation.Presentation : evaluation.Taste)
            });
            return bestEvaluations.OrderByDescending(x => x.Score).First().Name;
        }

        public static string GetBestOverall(List<Recipe> recipes)
        {
            var bestEvaluations = recipes.Select(x => new BestEvaluation
            {
                Name = x.Name,
                Score = x.Evaluations.Sum(evaluation => evaluation.Presentation + evaluation.Taste)
            });
            return bestEvaluations.OrderByDescending(x => x.Score).First().Name;
        }

        public static void Start()
        {
            int countOfRecipes = int.Parse(Console.ReadLine());

            var recipes = new List<Recipe>();
            for (int i = 0; i < countOfRecipes; i++)
            {
                string name = Console.ReadLine();
                var currentRecipe = new Recipe
                {
                    Name = name,
                    Evaluations = new List<Evaluation>()
                };
                var scores = Console.ReadLine();
                var scoreArray = scores.Split(':').ToList();
                scoreArray.ForEach(score =>
                {
                    var presentation = score.Split(',')[0];
                    var taste = score.Split(',')[1];
                    currentRecipe.Evaluations.Add(new Evaluation
                    {
                        Presentation = int.Parse(presentation),
                        Taste = int.Parse(taste)
                    });
                });
                recipes.Add(currentRecipe);
            }

            Console.WriteLine($"Highest rating in presentation is {GetHighestRating(recipes, "Presentation")}");
            Console.WriteLine($"Highest rating in taste is {GetHighestRating(recipes, "Taste")}");
            Console.WriteLine($"Highest rating overall is {GetBestOverall(recipes)}");
        }
    }

    public class Recipe
    {
        public string Name { get; set; }
        public List<Evaluation> Evaluations { get; set; }
    }

    public class Evaluation
    {
        public int Taste { get; set; }
        public int Presentation { get; set; }
    }
}
