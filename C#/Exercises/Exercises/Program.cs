﻿using System;
using System.Collections.Generic;
using System.Linq;
using Exercises.CesarCypher;
using Exercises.FoodManagement;
using Exercises.QuoteManagement;

namespace Solution
{
    public class Solution
    {
        public static void Main()
        {
            //QuoteManagement.Start();
            //CesarCypher.Start();
            FoodManagement.Start();
        }
    }
}