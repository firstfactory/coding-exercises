﻿using System;
using System.Collections.Generic;

namespace Exercises.QuoteManagement
{
    public enum ProductCategory
    {
        Food,
        Appliance,
        Technology
    }

    public static class QuoteManagement
    {
        public class ItemSoldEventArgs : EventArgs
        {
            public string Code { get; set; }
        }

        public class Inventory
        {
            public List<IBaseProduct> Items { get; set; } = new List<IBaseProduct>();

            public void LoadItem(string code, string name, double price, int quantity, ProductCategory category)
            {
                switch (category)
                {
                    case ProductCategory.Appliance:
                        Items.Add(new ApplianceProduct
                        {
                            Code = code,
                            Name = name,
                            Price = price,
                            Quantity = quantity
                        });
                        break;
                    case ProductCategory.Technology:
                        Items.Add(new TechnologyProduct
                        {
                            Code = code,
                            Name = name,
                            Price = price,
                            Quantity = quantity
                        });
                        break;
                    case ProductCategory.Food:
                        Items.Add(new FoodProduct
                        {
                            Code = code,
                            Name = name,
                            Price = price,
                            Quantity = quantity
                        });
                        break;
                }
            }

            public void Sell(string code, int quantity)
            {
                var item = Items.Find(x => x.Code.Equals(code));
                var available = item.Quantity - item.Sold;
                if (available == 0 || available < quantity) return;
                item.Sold += quantity;
                SellEvent?.Invoke(this, new ItemSoldEventArgs()
                {
                    Code = code
                });
            }

            // Custom code
            public event EventHandler<ItemSoldEventArgs> SellEvent;
        }

        public class Clerk : IClerk
        {
            private Inventory _inventory;
            public Clerk(Inventory inventory)
            {
                _inventory = inventory;
            }

            public void Sell(string code, int quantity)
            {
                _inventory.Sell(code, quantity);
            }
        }

        public class Manager : IManager
        {
            private Inventory inventory;
            private ProductCategory preference;
            public Manager(Inventory inventory)
            {
                this.inventory = inventory;
                this.inventory.SellEvent += Sell_Event;
            }

            public void Sell_Event(object sender, ItemSoldEventArgs args)
            {
                var item = inventory.Items.Find(x => x.Code == args.Code);
                if (item.Quantity == item.Sold) return;
                var type = item.GetType();
                switch (preference)
                {
                    case ProductCategory.Appliance:
                        if (type.Name == "ApplianceProduct") item.UpdatePrice();
                        break;
                    case ProductCategory.Technology:
                        if (type.Name == "TechnologyProduct") item.UpdatePrice();
                        break;
                    case ProductCategory.Food:
                        if (type.Name == "FoodProduct") item.UpdatePrice();
                        break;
                }
            }

            public void GetInventory()
            {
                Console.WriteLine("Inventory is:");
                inventory.Items.ForEach(x =>
                {
                    Console.WriteLine($"Code: {x.Code} | Price: {x.Price} | Quantity: {x.Quantity - x.Sold}");
                });
            }

            public void SetPreference(ProductCategory productCategory)
            {
                preference = productCategory;
            }
        }

        public class FoodProduct : IBaseProduct
        {
            public string Code { get; set; }
            public string Name { get; set; }
            public double Price { get; set; }
            public int Quantity { get; set; }
            public int Sold { get; set; }

            public void UpdatePrice()
            {
                if (Sold >= Quantity / 2)
                {
                    Price *= 1.25;
                }
            }
        }

        public class ApplianceProduct : IBaseProduct
        {
            public string Code { get; set; }
            public string Name { get; set; }
            public double Price { get; set; }
            public int Quantity { get; set; }
            public int Sold { get; set; }

            public void UpdatePrice()
            {
                if (Sold == Quantity - 1)
                {
                    Price *= 1.50;
                }
            }
        }

        public class TechnologyProduct : IBaseProduct
        {
            public string Code { get; set; }
            public string Name { get; set; }
            public double Price { get; set; }
            public int Quantity { get; set; }
            public int Sold { get; set; }

            public void UpdatePrice()
            {
                if (Sold >= Quantity / 2)
                {
                    Price *= 0.75;
                }
            }
        }

        public class Store
        {
            private Inventory inventory;
            public Store(Inventory inventory)
            {
                this.inventory = inventory;
            }

            public IManager GetManager()
            {
                return new Manager(inventory);
            }

            public IClerk GetClerk()
            {
                return new Clerk(inventory);
            }
        }

        public interface IBaseProduct
        {
            string Code { get; set; }
            string Name { get; set; }
            double Price { get; set; }
            int Quantity { get; set; }
            int Sold { get; set; }
            void UpdatePrice();
        }

        public interface IManager
        {
            void GetInventory();
            void SetPreference(ProductCategory productCategory);
        }

        public interface IClerk
        {
            void Sell(string code, int quantity);
        }

        public static void Start()
        {
            var countOfItems = int.Parse(Console.ReadLine());
            var inventory = new Inventory();
            for (var i = 0; i < countOfItems; i++)
            {
                var itemLine = Console.ReadLine();
                var itemArray = itemLine.Split(',');
                inventory.LoadItem(
                    itemArray[0]
                    , itemArray[1]
                    , double.Parse(itemArray[2])
                    , int.Parse(itemArray[3])
                    , (ProductCategory)Enum.Parse(typeof(ProductCategory), itemArray[4]));
            }
            var store = new Store(inventory);
            var clerk = store.GetClerk();
            var manager = store.GetManager();
            var preference = Console.ReadLine();
            manager.SetPreference(
                (ProductCategory)Enum.Parse(
                    typeof(ProductCategory)
                    , preference));
            var countOfSells = int.Parse(Console.ReadLine());
            for (var i = 0; i < countOfSells; i++)
            {
                var sellLine = Console.ReadLine();
                var sellArray = sellLine.Split(',');
                clerk.Sell(sellArray[0], int.Parse(sellArray[1]));
            }
            manager.GetInventory();
        }
    }
}