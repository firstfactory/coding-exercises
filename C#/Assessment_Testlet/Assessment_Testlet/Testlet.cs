﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assessment_Testlet
{
    public class Testlet
    {
        public string TestletId;
        private List<Item> Items;

        public Testlet(string testletId, List<Item> items)
        {
            TestletId = testletId;
            Items = items;
        }
        public List<Item> Randomize()
        {
            //Items private collection has 6 Operational and 4 Pretest Items.
            // Randomize the order of these items as per the requirement(with TDD)
            //The assignment will be reviewed on the basis of – Tests written first, Correct
            //logic, Well structured &clean readable code.

            List<Item> list = this.Items;
            int firstItems = 0;
            int counter = 0;

            // Randomize the order of the list
            List<Item> alternatingOrder = list
                    .Select((i, index) => new
                    {
                        i,
                        Margin = index < list.Count / 2 ? index : list.Count - ++index
                    })
                    .OrderBy(x => x.Margin)
                    .Select(x => x.i)
                    .ToList();


            //Sorting initial pretests
            while (alternatingOrder.Count > 0)
            {
                if(firstItems < 2) { 
                    if (alternatingOrder[counter].ItemType == Item.ItemTypeEnum.Pretest)
                    {
                        firstItems++;
                        counter++;
                    }
                    else
                    {
                        Item swapped = alternatingOrder[counter];

                        alternatingOrder.RemoveAt(counter);
                        alternatingOrder.Add(swapped);
                    }
                }
                else
                {
                    break;
                }
            }

            return alternatingOrder;
        }

       
    }
}
