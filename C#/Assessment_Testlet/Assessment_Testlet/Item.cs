﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assessment_Testlet
{
    public class Item
    {
        public string ItemId;
        public ItemTypeEnum ItemType;

        public enum ItemTypeEnum
        {
            Pretest = 0,
            Operational = 1
        }
    }
}
