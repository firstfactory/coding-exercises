﻿using System;
using System.Collections.Generic;

namespace Assessment_Testlet
{
    class Program
    {
        static void Main(string[] args)
        {
            //New list (Mock data)
            List<Item> newList = Utilities.GenerateList();

            //Shows generated list.
            Utilities.ShowList("Original list", newList);

            //Creates Testlet.
            Testlet t1 = new Testlet("Testlet 1", newList);

            //Executing
            Utilities.ShowList(string.Format("Random list from {0}", t1.TestletId), t1.Randomize());

        }
    }
}
