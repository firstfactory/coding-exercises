﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assessment_Testlet
{
    internal static class Utilities
    {
        public static List<Item> GenerateList()
        {
            List<Item> items = new List<Item>();

            for (int i = 0; i < 10; i++)
            {
                Item item = new Item();
                item.ItemId = string.Format("Question: {0}.", Guid.NewGuid().ToString());

                if (i < 6)
                {
                    item.ItemType = Item.ItemTypeEnum.Operational;
                }
                else
                {
                    item.ItemType = Item.ItemTypeEnum.Pretest;
                }

                items.Add(item);
            }

            return items;
        }

        public static void ShowList(string Label, List<Item> items)
        {
            Console.WriteLine(Label);
            foreach (Item item in items)
            {
                Console.WriteLine(string.Format("Item id {0}, Item type {1}", item.ItemId, item.ItemType));
            }
        }
    }
}
