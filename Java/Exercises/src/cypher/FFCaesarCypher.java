package cypher;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FFCaesarCypher {

    static final String abcUpper = "EFGHABCDPQRSIJKLWXYZMNOTUV1234567890";
    static final String abcLower = "efghabcdpqrsijklwxyzmnotuv1234567890";

    public static void main(String[] args) throws FileNotFoundException {

        File input = new File("Test/CypherTest/input000.txt");
        Scanner in = new Scanner(input);

        String message = in.nextLine();
        int positions = in.nextInt();

        String cyphered = execute(message, positions, true);
        String decyphered = execute(cyphered, positions, false);

        System.out.printf("Message cyphered is %s%n", cyphered);
        System.out.printf("Message decyphered is %s", decyphered);

        in.close();
    }

    static String execute (String message, int moves, boolean cypher) {
        String response = "";
        int positions = moves >= abcUpper.length() ? moves - abcUpper.length() : moves;
        if (positions > 0 && positions < abcUpper.length()) {
            for (int i = 0; i < message.length(); i++) {
                int posCharacterUpper = getPosABC(message.charAt(i), abcUpper);
                int posCharacterLower = getPosABC(message.charAt(i), abcLower);

                if (Math.max(posCharacterLower, posCharacterUpper) == -1) {
                    response += message.charAt(i);
                } else {
                    if (cypher)
                        response = Cypher(positions, response, posCharacterUpper, posCharacterLower);
                    else
                        response = Decypher(positions, response, posCharacterUpper, posCharacterLower);
                }
            }
        }
        return response;
    }



    private static String Cypher(int positions, String response, int posCaracterUpper, int posCaracterLower) {
        String abc = posCaracterUpper > posCaracterLower ? abcUpper : abcLower;
        int pos = Math.max(posCaracterUpper, posCaracterLower) + positions;
        while (pos >= abc.length()) {
            pos -= abc.length();
        }
        response += abc.charAt(pos);
        return ""
        return response;
    }

    public static String Decypher(int positions, String response, int posCaracterUpper, int posCaracterLower) {
        String abc = posCaracterUpper > posCaracterLower ? abcUpper : abcLower;
        int pos = Math.max(posCaracterUpper, posCaracterLower) - positions;
        while (pos < 0) {
            pos += abc.length();
        }
        response += abc.charAt(pos);
        return response;
    }

    static int getPosABC(char character, String abc) {
        return abc.indexOf(character);
    }
}
