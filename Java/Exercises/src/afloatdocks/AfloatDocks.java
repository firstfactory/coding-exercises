package afloatdocks;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AfloatDocks {

    public static final double AFLOAT_PRICE =  23200.00;
    public static final double IN_DOCKS_PRICE =  15200.00;
    public static final double LEASING_LIMIT =  100000.00;

    public static final int AFLOAT_CONSTANT = 0;
    public static final int IN_DOCKS_CONSTANT = 1;

    public static void main(String[] args) throws FileNotFoundException {
        File file = new File ("Test/AfloatDocks/input002.txt");
        Scanner in = new Scanner(file);

        int countOfItems = in.nextInt();
        in.nextLine(); // flush

        RepairData repairData = new RepairData();
        for (int idx = 0; idx < countOfItems; idx++) {
            String itemLine = in.nextLine();
            String[] itemsArray = itemLine.split(",");
            repairData.loadRepairsData(Integer.parseInt(itemsArray[0]),
                    itemsArray[1],
                    itemsArray[2],
                    itemsArray[3],
                    RepairType.valueOf(itemsArray[4]));
        }

        String leasingInfoLine = in.nextLine();
        String[] leasingInfoArray = leasingInfoLine.split(",");
        String leasingId = leasingInfoArray[0];
        String leasingName = leasingInfoArray[1];



        String bitcoinInfoLine = in.nextLine();
        String[] bitcoinInfoArray = bitcoinInfoLine.split(",");
        String bitcoinId = bitcoinInfoArray[0];
        String bitcoinPassword = bitcoinInfoArray[1];

        int[] typeOfRepairsCounter = new int[2];
        for (IRepairDetails repairDetails:
             repairData.repairDetails) {

            RepairType repairType = RepairType.valueOf(repairDetails.getClass().getSimpleName());
            switch (repairType) {
                case Afloat:
                    typeOfRepairsCounter[AFLOAT_CONSTANT]++;
                    break;
                case InDocks:
                    typeOfRepairsCounter[IN_DOCKS_CONSTANT]++;
                    break;
            }
        }

        PaymentMethod paymentMethod;
        if (!isLeasingPayable(typeOfRepairsCounter)) {
            paymentMethod = new BitcoinPay(bitcoinId, bitcoinPassword);
        } else {
            paymentMethod = new LeasingPay(leasingId, leasingName);
        }

        paymentMethod.getRepairDetails(
                typeOfRepairsCounter[AFLOAT_CONSTANT],
                typeOfRepairsCounter[IN_DOCKS_CONSTANT]);
        in.close();

    }

    public static boolean isLeasingPayable(int[] typeOfRepairsCounter) {
        double total = (typeOfRepairsCounter[AFLOAT_CONSTANT] * AFLOAT_PRICE)
                + (typeOfRepairsCounter[IN_DOCKS_CONSTANT] * IN_DOCKS_PRICE);
        return total < LEASING_LIMIT;
    }

    public static class LeasingPay implements PaymentMethod {

        private final String companyId;
        private final String companyName;

        public LeasingPay(String companyId, String companyName) {
            this.companyId = companyId;
            this.companyName = companyName;
        }

        @Override
        public boolean isPaymentSent(String paymentInfo) {
            return true;
        }

        @Override
        public void getRepairDetails(int afloatRepairs, int inDocksRepairs) {
            System.out.printf("The payment has been successfully sent using company's %s-%s leasing.%n",
                    companyId, companyName);
            System.out.printf("Paid %d afloat repairs,%nand %d in docks repairs.%n",
                    afloatRepairs, inDocksRepairs);
        }
    }

    public static class BitcoinPay implements PaymentMethod {
        private final String walletId;
        private final String walletPassword;

        public BitcoinPay(String walletId, String walletPassword) {
            this.walletId = walletId;
            this.walletPassword = walletPassword;
        }

        @Override
        public boolean isPaymentSent(String paymentInfo) {
            if (paymentInfo.substring(1, paymentInfo.length() - 2).length() > 8) {
                return true;
            }
            System.out.println("Invalid wallet id or password, transaction has not been authorized.");
            return false;
        }

        @Override
        public void getRepairDetails(int afloatRepairs, int inDocksRepairs) {
            if (isPaymentSent(walletPassword)) {
                System.out.printf("The payment has been successfully sent using %s%n", walletId);
                System.out.printf("Paid %d afloat repairs,%nand %d in docks repairs.%n",
                        afloatRepairs, inDocksRepairs);
            }
        }
    }

    public interface PaymentMethod {
        boolean isPaymentSent(String paymentInfo);
        void getRepairDetails(int afloatRepairs, int inDocksRepairs);
    }

    public interface IRepairDetails {
        int getCode();
        String getModel();
        String getLatitude();
        String getLongitude();

    }
    public static class InDocks implements IRepairDetails {
        private final int code;
        private final String model;
        private final String latitude;
        private final String longitude;

        public InDocks(int code, String model, String latitude, String longitude) {
            this.code = code;
            this.model = model;
            this.latitude = latitude;
            this.longitude = longitude;
        }

        @Override public int getCode() { return code; }
        @Override public String getModel() { return model; }
        @Override public String getLatitude() { return latitude; }
        @Override public String getLongitude() { return longitude; }
    }

    public static class Afloat implements IRepairDetails{

        private final int code;
        private final String model;
        private final String latitude;
        private final String longitude;

        public Afloat(int code, String model, String latitude, String longitude) {
            this.code = code;
            this.model = model;
            this.latitude = latitude;
            this.longitude = longitude;
        }

        @Override public int getCode() { return code; }
        @Override public String getModel() { return model; }
        @Override public String getLatitude() { return latitude; }
        @Override public String getLongitude() { return longitude; }
    }

    public static class RepairData {
        List<IRepairDetails> repairDetails = new ArrayList<>();

        public void loadRepairsData(int code,
                                    String model,
                                    String latitude,
                                    String longitude,
                                    RepairType repairType) {
            switch (repairType) {
                case Afloat:
                    repairDetails.add(new Afloat(code, model, latitude, longitude));
                    break;
                case InDocks:
                    repairDetails.add(new InDocks(code, model, latitude, longitude));
                    break;
            }

        }
    }
}

enum RepairType {
    Afloat,
    InDocks
}
