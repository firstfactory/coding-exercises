package food;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Food {

    public static void main(String[] args) throws FileNotFoundException {

        File input = new File ("Test/FoodTest/input000.txt");
        Scanner in = new Scanner(input);

        int countOfRecipes = in.nextInt();
        in.nextLine(); // flush
        List<Recipe> recipes = new ArrayList<Recipe>();
        for (int i = 0; i < countOfRecipes; i++) {
            String name = in.nextLine();
            Recipe currentRecipe = new Recipe(name, new ArrayList<>());
            String scores = in.nextLine();
            String[] scoreArray = scores.split(":");
            for (String score :
                    scoreArray) {
                String presentation = score.split(",")[0];
                String taste = score.split(",")[1];
                currentRecipe.evaluations.add(new Evaluation(
                        Integer.parseInt(presentation),
                        Integer.parseInt(taste))
                );

            }
            recipes.add(currentRecipe);
        }

        // write your code here
        double maxPresentation = 0.0, maxTaste = 0.0, maxOverall = 0.0;
        String maxPresentationStr = "", maxTasteStr = "", maxOverallStr = "";

        for (Recipe recipe : recipes) {
            List<Evaluation> evaluations = recipe.getEvaluations();

            double presentationEvaluation  = evaluations.stream()
                    .mapToDouble(Evaluation::getPresentation)
                    .average().orElse(Double.NaN);
            double tasteEvaluation  = evaluations.stream()
                    .mapToDouble(Evaluation::getTaste)
                    .average().orElse(Double.NaN);
            double overall = presentationEvaluation + tasteEvaluation;

            if (presentationEvaluation > maxPresentation) {
                maxPresentation = presentationEvaluation;
                maxPresentationStr = recipe.name;
            }
            if (tasteEvaluation > maxTaste) {
                maxTaste = tasteEvaluation;
                maxTasteStr = recipe.name;
            }
            if (overall > maxOverall) {
                maxOverall = overall;
                maxOverallStr = recipe.name;
            }
        }

        System.out.printf("Highest rating in presentation is %s%n", maxPresentationStr);
        System.out.printf("Highest rating in taste is %s%n", maxTasteStr);
        System.out.printf("Highest rating overall is %s", maxOverallStr);

        in.close();
    }

    public static class Recipe {
        public String name;
        public List<Evaluation> evaluations;

        public Recipe(String name, List<Evaluation> evaluations) {
            this.name = name;
            this.evaluations = evaluations;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<Evaluation> getEvaluations() {
            return evaluations;
        }

        public void setEvaluations(List<Evaluation> evaluations) {
            this.evaluations = evaluations;
        }
    }

    public static class Evaluation {
        public int presentation;
        public int taste;

        public Evaluation(int presentation, int taste) {
            this.presentation = presentation;
            this.taste = taste;
        }

        public int getTaste() {
            return taste;
        }

        public void setTaste(int taste) {
            this.taste = taste;
        }

        public int getPresentation() {
            return presentation;
        }

        public void setPresentation(int presentation) {
            this.presentation = presentation;
        }

    }
}
