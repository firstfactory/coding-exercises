package quote;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.HashMap;


public class Quote {
    public static void main(String[] args) throws FileNotFoundException {

        File file = new File ("Test/QuoteTest/input005.txt");
        Scanner in = new Scanner(file);

        int countOfItems = in.nextInt();
        in.nextLine(); // flush
        Inventory inventory = new Inventory();

        for (int idx = 0; idx < countOfItems; idx++) {
            String itemLine = in.nextLine();
            String[] itemArray = itemLine.split(",");
            inventory.loadItem(
                    itemArray[0],
                    itemArray[1],
                    Double.parseDouble(itemArray[2]),
                    Integer.parseInt(itemArray[3]),
                    ProductCategory.valueOf(itemArray[4]));

        }
        // write your code here
        Store store = new Store(inventory);
        Clerk clerk = (Clerk) store.getClerk();
        Manager manager = (Manager) store.getManager();

        String preference = in.nextLine();
        manager.setPreference(ProductCategory.valueOf(preference));

        inventory.events.subscribe("sell", manager);

        int countOfSells = in.nextInt();
        in.nextLine();
        for (int idx = 0; idx < countOfSells; idx++) {
            String sellLine = in.nextLine();
            String[] sellsArray = sellLine.split(",");
            clerk.sell(sellsArray[0], Integer.parseInt(sellsArray[1]));

        }

        manager.getInventory();
        in.close();
    }

    public static class Store {
        private final Inventory inventory;
        public Store(Inventory inventory) {
            this.inventory = inventory;
        }

        public IManager getManager() {
            return new Manager(inventory);
        }

        public IClerk getClerk() {
            return new Clerk(inventory);
        }
    }

    public static class Inventory {
        public List<IBaseProduct> items = new ArrayList<>();
        public EventManager events;
        public Inventory() {
            this.events = new EventManager("sell");
        }
        public void loadItem(String code, String name, double price, int quantity, ProductCategory category) {
            switch (category) {
                case Appliance:
                    items.add(new Appliance(code, name, price, quantity));
                break;
                case Technology:
                    items.add(new Technology(code, name, price, quantity));
                break;
                case Food:
                    items.add(new Food(code, name, price, quantity));
                break;
            }
        }

        public void sell(String code, int quantity) {

            IBaseProduct matchingItem = items
                    .stream()
                    .filter(itemStream -> itemStream.getCode().equals(code))
                    .findFirst().orElse(null);

            int sold = matchingItem.getSold();
            int available = matchingItem.getQuantity() - sold;
            if (available == 0 || available < quantity)
                return;
            matchingItem.setSold(sold + quantity);
            events.notify("sell", matchingItem);
        }
    }

    public static class Manager implements IManager, IEventListener {

        private final Inventory inventory;
        private ProductCategory preference;

        public Manager(Inventory inventory) {
            this.inventory = inventory;
        }

        @Override
        public void getInventory() {
            System.out.println("Inventory is:");
            inventory.items
                    .forEach(iBaseProduct -> System.out.printf("Code: %s | Price: %f | Quantity: %d%n",
                            iBaseProduct.getCode(),
                            iBaseProduct.getPrice(),
                            iBaseProduct.getQuantity() - iBaseProduct.getSold()));
        }

        @Override
        public void setPreference(ProductCategory productCategory) {
            this.preference = productCategory;
        }

        @Override
        public void update(String eventType, IBaseProduct item) {

            if (item.getQuantity() == item.getSold())
                return;
            ProductCategory type = ProductCategory.valueOf(item.getClass().getSimpleName());
            switch (type) {
                case Food:
                    if (preference == ProductCategory.Food)
                        item.updatePrice();
                    break;
                case Appliance:
                    if (preference == ProductCategory.Appliance)
                        item.updatePrice();
                    break;
                case Technology:
                    if (preference == ProductCategory.Technology)
                        item.updatePrice();
                    break;
            }
        }
    }
    public static class Clerk implements IClerk {
        private final Inventory inventory;
            public Clerk(Inventory inventory) {
            this.inventory = inventory;
        }

        @Override
        public void sell(String code, int quantity) {
           inventory.sell(code, quantity);
        }
    }

    public interface IBaseProduct {
        void updatePrice();
        String getCode();
        String getName();
        double getPrice();
        int getQuantity();
        int getSold();
        void setSold(int sold);
    }

    public interface IManager {
        void getInventory();
        void setPreference(ProductCategory productCategory);
    }

    public interface IClerk {
        void sell(String code, int quantity);
    }

    public static class Food implements IBaseProduct {
        public String code, name;
        public double price;
        public int quantity, sold;

        public Food(String code, String name, double price, int quantity) {
            this.code = code;
            this.name = name;
            this.price = price;
            this.quantity = quantity;
        }

        @Override
        public void updatePrice() {
            if (sold >= quantity / 2) {
                price *= 1.25;
            }
        }

        @Override public String getCode() { return code; }
        @Override public String getName() { return name; }
        @Override public double getPrice() { return price; }
        @Override public int getQuantity() { return quantity; }
        @Override public int getSold() { return sold; }
        @Override public void setSold(int sold) { this.sold = sold; }
    }

    public static class Appliance implements IBaseProduct {
        public String code, name;
        public double price;
        public int quantity, sold;

        public Appliance(String code, String name, double price, int quantity) {
            this.code = code;
            this.name = name;
            this.price = price;
            this.quantity = quantity;
        }

        @Override
        public void updatePrice() {
            if (sold == quantity - 1) {
                price *= 1.50;
            }
        }

        @Override public String getCode() { return code; }
        @Override public String getName() { return name; }
        @Override public double getPrice() { return price; }
        @Override public int getQuantity() { return quantity; }
        @Override public int getSold() { return sold; }
        @Override public void setSold(int sold) { this.sold = sold; }
    }

    public static class Technology implements IBaseProduct {
        public String code, name;
        public double price;
        public int quantity, sold;

        public Technology(String code, String name, double price, int quantity) {
            this.code = code;
            this.name = name;
            this.price = price;
            this.quantity = quantity;
        }

        @Override
        public void updatePrice() {

            if (sold >= quantity / 2) {
                price *= 0.75;
            }
        }

        @Override public String getCode() { return code; }
        @Override public String getName() { return name; }
        @Override public double getPrice() { return price; }
        @Override public int getQuantity() { return quantity; }
        @Override public int getSold() { return sold; }
        @Override public void setSold(int sold) { this.sold = sold; }
    }

}

class EventManager {
    Map<String, List<IEventListener>> listeners = new HashMap<>();
    public EventManager(String... operations) {
        for (String operation : operations) {
            this.listeners.put(operation, new ArrayList<>());
        }

    }
    public void subscribe(String eventType, IEventListener listener) {
        List<IEventListener> users = listeners.get(eventType);
        users.add(listener);
    }

    public void notify(String eventType, Quote.IBaseProduct itemCode) {
        List<IEventListener> users = listeners.get(eventType);
        for (IEventListener listener : users) {
            listener.update(eventType, itemCode);
        }
    }
}

interface IEventListener {
    void update(String eventType, Quote.IBaseProduct item);
}

enum ProductCategory {
    Food,
    Appliance,
    Technology
}